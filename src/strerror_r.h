/***********************************************************************\
 * Libcrash is a library to aid in debugging programs when they crash. *
 * Copyright (C) 2019  Asher Gordon <AsDaGo@posteo.net>                *
 *                                                                     *
 * This file is part of libcrash.                                      *
 *                                                                     *
 * Libcrash is free software: you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published   *
 * by the Free Software Foundation, either version 3 of the License,   *
 * or (at your option) any later version.                              *
 *                                                                     *
 * Libcrash is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of      *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *
 * GNU General Public License for more details.                        *
 *                                                                     *
 * You should have received a copy of the GNU General Public License   *
 * along with libcrash.  If not, see <https://www.gnu.org/licenses/>.  *
\***********************************************************************/

/* strerror_r.h -- portable definition of strerror_r() */

#ifndef _STRERROR_R_H
#define _STRERROR_R_H

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include <string.h>

#ifdef HAVE_STRERROR_R
# if !HAVE_DECL_STRERROR_R
#  ifdef STRERROR_R_CHAR_P
char *strerror_r(int, char *, size_t);
#  else /* !STRERROR_R_CHAR_P */
int strerror_r(int, char *, size_t);
#  endif /* !STRERROR_R_CHAR_P */
# endif /* !HAVE_DECL_STRERROR_R */
#else /* !HAVE_STRERROR_R */
/* Fall back to the unsafe strerror(). */
# define strerror_r(errnum, buf, buflen)	\
  strncpy(buf, strerror(errnum), buflen)

/* We now have a working strerror_r(). */
# define HAVE_STRERROR_R 1

/* The above definition returns a pointer to `buf' which is the same
   behavior as the GNU version. */
# define STRERROR_R_CHAR_P 1
#endif /* !HAVE_STRERROR_R */

#endif /* !_STRERROR_R_H */
