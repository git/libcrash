/***********************************************************************\
 * Libcrash is a library to aid in debugging programs when they crash. *
 * Copyright (C) 2019  Asher Gordon <AsDaGo@posteo.net>                *
 *                                                                     *
 * This file is part of libcrash.                                      *
 *                                                                     *
 * Libcrash is free software: you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published   *
 * by the Free Software Foundation, either version 3 of the License,   *
 * or (at your option) any later version.                              *
 *                                                                     *
 * Libcrash is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of      *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *
 * GNU General Public License for more details.                        *
 *                                                                     *
 * You should have received a copy of the GNU General Public License   *
 * along with libcrash.  If not, see <https://www.gnu.org/licenses/>.  *
\***********************************************************************/

/* crash.h -- print debugging message when we receive a signal that
   indicates a bug */

#ifndef _CRASH_H
#define _CRASH_H

#include <stddef.h>

typedef void (*crash_hook_t)(int);

int crash_init(const char *, const char *, const char *, const char *);
int crash_init_signals(const char *, const char *, const char *, const char *,
		       const int *, size_t);
void crash_hook(crash_hook_t);

#endif /* !_CRASH_H */
