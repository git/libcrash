/***********************************************************************\
 * Libcrash is a library to aid in debugging programs when they crash. *
 * Copyright (C) 2019  Asher Gordon <AsDaGo@posteo.net>                *
 *                                                                     *
 * This file is part of libcrash.                                      *
 *                                                                     *
 * Libcrash is free software: you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published   *
 * by the Free Software Foundation, either version 3 of the License,   *
 * or (at your option) any later version.                              *
 *                                                                     *
 * Libcrash is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of      *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *
 * GNU General Public License for more details.                        *
 *                                                                     *
 * You should have received a copy of the GNU General Public License   *
 * along with libcrash.  If not, see <https://www.gnu.org/licenses/>.  *
\***********************************************************************/

/* core.c -- dump core */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <errno.h>

#include "core.h"
#include "io.h"
#include "strerror_r.h"

/* Read until newline or EOF */
#define flush_stdin()				\
  while (!(c == EOF || c == '\n'))		\
    c = dgetc(STDIN_FILENO)

/* Dump core. Does not return if core dump is successfully, returns
   nonzero on error (and errno will be set), and returns zero if
   coredump was user-aborted. */
int dump_core(void) {
  struct rlimit corelim;
  int answer, c;
  char errstr[1024];

  /* Ask if we should generate a core dump */
  dputs("Dump core? ", STDERR_FILENO);

  c = answer = dgetc(STDIN_FILENO);
  flush_stdin();

  if (answer != 'y' && answer != 'Y')
    return 0;

  /* Get the core size limit */
  if (getrlimit(RLIMIT_CORE, &corelim)) {
    strerror_r(errno, errstr, sizeof(errstr));

    dputs("Unable to get core size limit: ", STDERR_FILENO);
    dputs(errstr, STDERR_FILENO);
    dputs("\nCore dump may fail.\n", STDERR_FILENO);
  }
  else {
    char set_corelim = 0; /* Whether to set the core size limit */

    if (!corelim.rlim_cur) {
      if (!corelim.rlim_max) {
	/* Both hard and soft limits are zero */

	dputs("Core dumps are hard disabled. Try to enable anyway? ",
	      STDERR_FILENO);

	c = answer = dgetc(STDIN_FILENO);
	flush_stdin();

	if (answer != 'y' && answer != 'Y')
	  return 0;

	corelim.rlim_cur = corelim.rlim_max = RLIM_INFINITY;
	set_corelim = 1;
      }
      else {
	/* Soft limit is zero but hard limit is not */

	dputs("Core dumps are disabled. Enable? ", STDERR_FILENO);

	c = answer = dgetc(STDIN_FILENO);
	flush_stdin();

	if (answer != 'y' && answer != 'Y')
	  return 0;

	if (corelim.rlim_max != RLIM_INFINITY) {
	  /* Hard limit is not zero but is not unlimited */

	  dputs("Core dump size is hard limited to ", STDERR_FILENO);
	  dputl(corelim.rlim_max, STDERR_FILENO);
	  dputs("B. Try to unlimit? ", STDERR_FILENO);

	  c = answer = dgetc(STDIN_FILENO);
	  flush_stdin();

	  if (answer == 'y' || answer == 'Y') {
	    rlim_t old_max = corelim.rlim_max;

	    corelim.rlim_cur = corelim.rlim_max = RLIM_INFINITY;
	    if (setrlimit(RLIMIT_CORE, &corelim)) {
	      strerror_r(errno, errstr, sizeof(errstr));

	      dputs("Unable to unlimit core size: ", STDERR_FILENO);
	      dputs(errstr, STDERR_FILENO);
	      dputs("\nFalling back to hard limit: ", STDERR_FILENO);
	      dputl(old_max, STDERR_FILENO);
	      dputs("B\n", STDERR_FILENO);

	      corelim.rlim_cur = corelim.rlim_max = old_max;
	      set_corelim = 1;
	    }
	  }
	  else {
	    corelim.rlim_cur = corelim.rlim_max;
	    set_corelim = 1;
	  }
	}
	else {
	  /* No hard limit */
	  corelim.rlim_cur = corelim.rlim_max;
	  set_corelim = 1;
	}
      }
    }
    else {
      /* Soft limit is not zero */

      if (corelim.rlim_cur != RLIM_INFINITY) {
	/* But it's not unlimited either */

	dputs("Core dump size is limited to ", STDERR_FILENO);
	dputl(corelim.rlim_cur, STDERR_FILENO);
	dputs("B. Unlimit? ", STDERR_FILENO);

	c = answer = dgetc(STDIN_FILENO);
	flush_stdin();

	if (answer == 'y' || answer == 'Y') {
	  if (corelim.rlim_max != RLIM_INFINITY) {
	    /* It is hard limited */

	    dputs("It is hard limited. Try to unlimit anyway? ",
		  STDERR_FILENO);

	    c = answer = dgetc(STDIN_FILENO);
	    flush_stdin();

	    if (answer == 'y' || answer == 'Y') {
	      rlim_t old_max = corelim.rlim_max;

	      corelim.rlim_cur = corelim.rlim_max = RLIM_INFINITY;
	      if (setrlimit(RLIMIT_CORE, &corelim)) {
		strerror_r(errno, errstr, sizeof(errstr));

		dputs("Unable to unlimit core size: ", STDERR_FILENO);
		dputs(errstr, STDERR_FILENO);
		dputs("\nFalling back to hard limit: ", STDERR_FILENO);
		dputl(old_max, STDERR_FILENO);
		dputs("B\n", STDERR_FILENO);

		corelim.rlim_cur = corelim.rlim_max = old_max;
		set_corelim = 1;
	      }
	    }
	  }
	  else {
	    /* It's not hard limited */
	    corelim.rlim_cur = corelim.rlim_max;
	    set_corelim = 1;
	  }
	}
      }
    }

    if (set_corelim && setrlimit(RLIMIT_CORE, &corelim)) {
      strerror_r(errno, errstr, sizeof(errstr));

      dputs("Unable to set core size limit to ", STDERR_FILENO);
      dputl(corelim.rlim_cur, STDERR_FILENO);
      dputs("B (hard limit ", STDERR_FILENO);
      dputl(corelim.rlim_max, STDERR_FILENO);
      dputs("B): ", STDERR_FILENO);
      dputs(errstr, STDERR_FILENO);
      dputs("\n", STDERR_FILENO);

      if (!getrlimit(RLIMIT_CORE, &corelim)) {
	dputs("Soft limit is ", STDERR_FILENO);
	dputl(corelim.rlim_cur, STDERR_FILENO);
	dputs("B and hard limit is ", STDERR_FILENO);
	dputl(corelim.rlim_max, STDERR_FILENO);
	dputs("B\n", STDERR_FILENO);
      }
    }
  }

  dputs("\nDumping core.\n\n"
	"Please note that core dumps may fail under certain conditions. "
	"The core dump\n"
	"will probably be called \"core\" or \"core.", STDERR_FILENO);
  dputl(getpid(), STDERR_FILENO);
  dputs("\". See the core(5) manpage for more\n"
	"info.\n", STDERR_FILENO);

  /* Finally dump core (or try to at least) */
  if (raise(SIGABRT))
    return 1;

  /* If we're still here, we failed to dump core */
  return -1;
}
