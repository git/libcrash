/***********************************************************************\
 * Libcrash is a library to aid in debugging programs when they crash. *
 * Copyright (C) 2019  Asher Gordon <AsDaGo@posteo.net>                *
 *                                                                     *
 * This file is part of libcrash.                                      *
 *                                                                     *
 * Libcrash is free software: you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published   *
 * by the Free Software Foundation, either version 3 of the License,   *
 * or (at your option) any later version.                              *
 *                                                                     *
 * Libcrash is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of      *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *
 * GNU General Public License for more details.                        *
 *                                                                     *
 * You should have received a copy of the GNU General Public License   *
 * along with libcrash.  If not, see <https://www.gnu.org/licenses/>.  *
\***********************************************************************/

/* io.c -- signal safe I/O */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include <stdio.h>
#include <unistd.h>
#include <string.h>

#include "io.h"

/* Write an integer to `fd' in an async-signal-safe manner. Returns 0
   on success, nonzero on error. */
int dputl(long num, int fd) {
  char c;
  long digit;
  long magnitude = 1;

  if (num < 0) {
    /* The number is negative. Write a minus sign... */
    c = '-';
    if (write(fd, &c, 1) < 1)
      return 1;

    /* ...and then the rest of the number. */
    return dputl(-num, fd);
  }

  /* Get the first digit. */
  for (digit = num; digit > 9; digit /= 10)
    magnitude *= 10;

  /* Write it. */
  c = digit + '0';
  if (write(fd, &c, 1) < 1)
    return 1;

  if (magnitude == 1) {
    /* We're done! */
    return 0;
  }

  /* Write the rest of the number. */
  return dputl(num - (digit * magnitude), fd);
}

/* Write a nul terminated string to `fd'. Returns 0 on success,
   nonzero on error. */
int dputs(const char *buf, int fd) {
  size_t count = strlen(buf);

  return (write(fd, buf, count) < count);
}

/* Get a single character from `fd'. Returns the character on success,
   EOF on error. */
int dgetc(int fd) {
  char c;

  if (read(fd, &c, 1) < 1)
    return EOF;

  return (int)c;
}
