/***********************************************************************\
 * Libcrash is a library to aid in debugging programs when they crash. *
 * Copyright (C) 2019  Asher Gordon <AsDaGo@posteo.net>                *
 *                                                                     *
 * This file is part of libcrash.                                      *
 *                                                                     *
 * Libcrash is free software: you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published   *
 * by the Free Software Foundation, either version 3 of the License,   *
 * or (at your option) any later version.                              *
 *                                                                     *
 * Libcrash is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of      *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *
 * GNU General Public License for more details.                        *
 *                                                                     *
 * You should have received a copy of the GNU General Public License   *
 * along with libcrash.  If not, see <https://www.gnu.org/licenses/>.  *
\***********************************************************************/

/* crash.c -- print debugging message when we receive a signal that
   indicates a bug */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <errno.h>

#include "crash.h"
#include "core.h"
#include "io.h"
#include "strerror_r.h"

#ifdef HAVE_BACKTRACE
# ifndef backtrace_size_t
/* Default to int */
#  define backtrace_size_t int
# endif /* backtrace_size_t */

# ifdef HAVE_EXECINFO_H
#  include <execinfo.h>
# else /* !HAVE_EXECINFO_H */
backtrace_size_t backtrace(void **, backtrace_size_t);
char **backtrace_symbols(void *const *, backtrace_size_t);
void backtrace_symbols_fd(void *const *, backtrace_size_t, int);
# endif /* !HAVE_EXECINFO_H */

/* The maximum backtrace size to print */
# define BACKTRACE_SIZE 100
#endif /* HAVE_BACKTRACE */

#include "sig2str.h"

/* Copy `b' to `a' if `b' is not NULL, and return 1 on error. */
#define COPY_STRING(a, b) do {						\
    if (b) {								\
      a = strdup(b);							\
      if (!a) {								\
	fprintf(stderr, "%s: cannot allocate space to copy \"%s\": %m\n", \
		progname, b);						\
	return 1;							\
      }									\
    }									\
  } while (0)

/* Information to put in the crash report. */
static char *package_string = NULL,
  *package_bugreport = NULL,
  *extra_info = NULL;

/* Which signals to print a debug message on. Note that if SIGSYS is
   not defined, the list will end with a trailing comma (assuming at
   least one of the other signals is defined). This has been legal in
   standard C for quite some time. */
static const int crash_signals[] =
  {
#ifdef SIGABRT
   SIGABRT,
#endif
#ifdef SIGBUS
   SIGBUS,
#endif
#ifdef SIGFPE
   SIGFPE,
#endif
#ifdef SIGILL
   SIGILL,
#endif
#ifdef SIGSEGV
   SIGSEGV,
#endif
#ifdef SIGSTKFLT
   SIGSTKFLT,
#endif
#ifdef SIGSYS
  SIGSYS
#endif
  };

#define CRASH_SIGNALS_ENTRIES (sizeof(crash_signals) / sizeof(*crash_signals))

#ifdef HAVE_ATEXIT
/* Free allocated memory on exit. */
static void free_memory(void) {
  if (package_string)
    free(package_string);

  if (package_bugreport)
    free(package_bugreport);

  if (extra_info)
    free(extra_info);
}
#endif /* HAVE_ATEXIT */

/* Hook to run right before we crash. */
static crash_hook_t run_at_crash = NULL;

/* The signal handler. */
static void __attribute__((__noreturn__)) crash_abort(int);

/* Call crash_init_signals() with the default signals. */
int crash_init(const char *progname, const char *package_string_local,
	       const char *package_bugreport_local,
	       const char *extra_info_local) {
  return crash_init_signals(progname, package_string_local,
			    package_bugreport_local, extra_info_local,
			    crash_signals, CRASH_SIGNALS_ENTRIES);
}

/* Initialize crash reporting (trap `signals'). `progname' is the name
   to use when reporting errors. `package_string' and
   `package_bugreport' will be initialized from `package_string_local'
   and `package_bugreport_local' respectively. Returns zero on
   success, nonzero on error. */
int crash_init_signals(const char *progname, const char *package_string_local,
		       const char *package_bugreport_local,
		       const char *extra_info_local,
		       const int *signals, size_t signals_entries) {
  struct sigaction action;

  if (signals_entries && !signals) {
    /* `signals' cannot be NULL if `signals_entries' is nonzero. */
    errno = EINVAL;
    fprintf(stderr, "%s: cannot initialize crash reporting: %m\n", progname);
    return 1;
  }

  /* Duplicate our local strings. Since we will need them for the
     entire scope of the program, they may never be free()'d if we
     were configured with --disable-free or we don't have atexit(). */
  COPY_STRING(package_string, package_string_local);
  COPY_STRING(package_bugreport, package_bugreport_local);
  COPY_STRING(extra_info, extra_info_local);

#ifdef HAVE_ATEXIT
  /* Free memory on exit. */
  if (atexit(free_memory)) {
    fprintf(stderr, "%s: cannot set exit function to free memory\n",
	    progname);
    return 1;
  }
#endif

  action.sa_handler = crash_abort;
  sigemptyset(&(action.sa_mask));
  action.sa_flags = SA_NODEFER; /* Since we will block all crash
				   signals (except SIGABRT) anyway and
				   we DON'T want to block SIGABRT (so
				   that we have a way to dump
				   core). */

  /* Add all the crash signals (except SIGABRT) to the signal mask */
  for (size_t i = 0; i < signals_entries; i++) {
    if (signals[i] != SIGABRT && sigaddset(&(action.sa_mask), signals[i])) {
      int errno_save = errno;
      char signame[SIG2STR_MAX];

      if (sig2str(signals[i], signame)) {
	fprintf(stderr,
		"%s: cannot add unknown signal (%d) to the signal mask: %s\n",
		progname, signals[i], strerror(errno_save));
      }
      else {
	fprintf(stderr, "%s: cannot add SIG%s (%d) to the signal mask: %s\n",
		progname, signame, signals[i], strerror(errno_save));
      }

      return 1;
    }
  }

  /* Trap signals which indicate bugs */
  for (size_t i = 0; i < signals_entries; i++) {
    if (sigaction(signals[i], &action, NULL)) {
      int errno_save = errno;
      char signame[SIG2STR_MAX];

      if (sig2str(signals[i], signame)) {
	fprintf(stderr, "%s: cannot trap unknown signal (%d): %s\n",
		progname, signals[i], strerror(errno_save));
      }
      else {
	fprintf(stderr, "%s: cannot trap SIG%s (%d): %s\n",
		progname, signame, signals[i], strerror(errno_save));
      }

      return 1;
    }
  }

  return 0;
}

/* Run a function on crash. */
void crash_hook(crash_hook_t hook) {
  run_at_crash = hook;
}

/* Print a crash report and exit(). This is a signal handler and it is
   not exported. */
static void __attribute__((__noreturn__)) crash_abort(int signum) {
  char signame[SIG2STR_MAX];
  struct sigaction action;
  char errstr[1024];
#ifdef HAVE_BACKTRACE
  void *bt_buf[BACKTRACE_SIZE];
  char **bt_str;
  backtrace_size_t bt_size;
#endif

  /* Reset SIGABRT to default (core dump) so that we have a way to
     dump core. Do it here rather than in dump_core() so that if we
     recieve SIGABRT again, we dump core right away, no questions
     asked. */
  action.sa_handler = SIG_DFL;
  sigemptyset(&(action.sa_mask));
  action.sa_flags = 0;

  if (sigaction(SIGABRT, &action, NULL))
    _exit(-signum);

  /* Run the user hook if any. */
  if (run_at_crash)
    run_at_crash(signum);

  dputs("Terminated by ", STDERR_FILENO);
  if (sig2str(signum, signame)) {
    dputs("unknown signal (", STDERR_FILENO);
    dputl(signum, STDERR_FILENO);
    dputs(")", STDERR_FILENO);
  }
  else {
    dputs("SIG", STDERR_FILENO);
    dputs(signame, STDERR_FILENO);
    dputs(" (", STDERR_FILENO);
    dputl(signum, STDERR_FILENO);
#ifdef HAVE_DECL_SYS_SIGLIST
    dputs(") ", STDERR_FILENO);
    dputs(sys_siglist[signum], STDERR_FILENO);
#else
    dputs(")", STDERR_FILENO);
#endif
  }
  dputs("\n\n", STDERR_FILENO);

  dputs("You've found a bug", STDERR_FILENO);
  if (package_string) {
    dputs(" in ", STDERR_FILENO);
    dputs(package_string, STDERR_FILENO);
  }
  dputs("!\n", STDERR_FILENO);

  if (package_bugreport) {
    dputs("Please report it to <", STDERR_FILENO);
    dputs(package_bugreport, STDERR_FILENO);
    dputs(">.\n", STDERR_FILENO);
  }

  dputs("\n", STDERR_FILENO);

  if (extra_info) {
    dputs(extra_info, STDERR_FILENO);
    dputs("\n\n", STDERR_FILENO);
  }

#ifdef HAVE_BACKTRACE
  /* Get the backtrace */
  bt_size = backtrace(bt_buf, BACKTRACE_SIZE);
  bt_str = backtrace_symbols(bt_buf, bt_size);

  if (!bt_str) {
    /* The GNU version (which we should have if we're running on GNU
       because we define _GNU_SOURCE in <config.h>) returns `char *'
       while the XSI version returns `int'. It doesn't matter which we
       have if we don't use the return value. */
    strerror_r(errno, errstr, sizeof(errstr));

    dputs("Could not allocate backtrace symbols: ", STDERR_FILENO);
    dputs(errstr, STDERR_FILENO);
    dputs("\nThis backtrace may not look as nice.\n", STDERR_FILENO);
  }

  dputs("Backtrace", STDERR_FILENO);
  if (bt_size >= BACKTRACE_SIZE)
    dputs(" (may be truncated)", STDERR_FILENO);
  dputs(":\n", STDERR_FILENO);

  if (bt_str) {
    /* Print the backtrace */
    for (char **cur_str = bt_str; bt_size; bt_size--, cur_str++) {
      /* This needs to hold at most as much as the whole backtrace,
	 and since the whole backtrace can't exceed the size of a
	 `backtrace_size_t', use that. */
      backtrace_size_t skipped = 0;

      dputs("#", STDERR_FILENO);
      dputl(bt_size, STDERR_FILENO);
      dputs(":\t", STDERR_FILENO);
      dputs(*cur_str, STDERR_FILENO);
      dputs("\n", STDERR_FILENO);

      /* Skip duplicate entries, but only if there is more than one
	 duplicate (this is useful since we have recursion). */
      if (bt_size > 2 && !strcmp(cur_str[1], cur_str[2])) {
	while (bt_size > 1 && !strcmp(cur_str[0], cur_str[1])) {
	  skipped++;

	  bt_size--;
	  cur_str++;
	}
      }

      /* Print how many we skipped if any */
      if (skipped) {
	dputs("[repeats ", STDERR_FILENO);
	dputl(skipped, STDERR_FILENO);
	dputs(" more times]\n", STDERR_FILENO);
      }
    }

    free(bt_str);
  }
  else {
    /* Fall back to backtrace_symbols_fd() */
    backtrace_symbols_fd(bt_buf, bt_size, STDERR_FILENO);
  }
#endif /* HAVE_BACKTRACE */

  dputs("\n", STDERR_FILENO);

  if (dump_core()) {
    strerror_r(errno, errstr, sizeof(errstr));

    dputs("Unable to generate core dump: ", STDERR_FILENO);
    dputs(errstr, STDERR_FILENO);
    dputs("\n", STDERR_FILENO);
  }

  exit(-signum);
}
